create user goran identified by ftn
	default tablespace USERS temporary tablespace TEMP;


	grant connect, resource to goran;

	grant create table to goran;

	grant create view to goran;

	grant create procedure to goran;

	grant create synonym to goran;

	grant create sequence to goran;

	grant select on dba_rollback_segs to goran;

	grant select on dba_segments to goran;

	grant unlimited tablespace to goran;